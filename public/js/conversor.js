let importe;

function muestraImporteConvertido(rates) {
    let importeConvertido = importe * rates.USD;
    let importeString = importe.toLocaleString('es-ES', {
        style: 'currency', 
        currency: 'EUR'
    });

    importeConvertidoString = importeConvertido.toLocaleString('us-US', {
        style: 'currency', 
        currency: 'USD'
    });

    Swal.fire({
        icon: 'success',
        title: 'Conversión correcta',
        text: importeString + ' equivalen a ' + importeConvertidoString
      });
}

function convierteImporte() {
    let http = new XMLHttpRequest();

    http.open('GET', 'https://api.exchangeratesapi.io/latest', true);

    http.send();

    http.addEventListener('readystatechange', function () {
        if (http.readyState === 4 && http.status === 200) {
            let conversiones = JSON.parse(http.responseText);

            muestraImporteConvertido(conversiones.rates);
        }
    });
}

function convertirImporte() {
    importe = document.getElementById('importe').value;

    importe = Number(importe);

    if (isNaN(importe) === true || importe === 0)
    {
        Swal.fire({
            icon: 'error',
            title: 'Importe incorrecto',
            text: "Debes introducir un importe numérico distinto de 0"
        });
    }
    else {
        convierteImporte();
    }
}

window.addEventListener('DOMContentLoaded', function () {
    let botonConvertir = document.getElementById('btConvertir');
    
    botonConvertir.addEventListener('click', convertirImporte);
});